from main import main


def test():
    assert float(main("F", 30, "C")) == -1.11
    assert float(main("K", 273.15, "C")) == 0
    assert main("i", 273.15, "C") == "Введена неверная начальная шкала"
    assert float(main("C", 10, "K")) == 283.15
    assert float(main("C", 40, "F")) == 104
    assert main("C", 60, "E") == "Введена неверная шкала назначения"
    assert float(main("F", 80, "K")) == 299.82
    assert float(main("K", 300, "F")) == 80.33



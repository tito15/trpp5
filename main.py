import sys
def toFixed(numObj, digits=0):
    return f"{numObj:.{digits}f}"


def main(inp, value, out):
    if inp == "C":
        if out == "F":
            return toFixed(value * 9 / 5 + 32, 2)
        elif (out == "K"):
            return toFixed(value + 273.15, 2)
        else:
            return "Введена неверная шкала назначения"
    elif inp == "F":
        if out == "C":
            return toFixed((value - 32) * 5 / 9, 2)
        elif out == "K":
            return toFixed((value - 32) * 5 / 9 + 273.15, 2)
        else:
            return "Введена неверная шкала назначения"
    elif inp == "K":
        if out == "C":
            return toFixed(value - 273.15, 2)
        elif out == "F":
            return toFixed((value - 273.15) * 9 / 5 + 32, 2)
        else:
            return "Введена неверная шкала назначения"
    else:
        return "Введена неверная начальная шкала"


if __name__ == '__main__':
    #print("Вводите данные в формате K,F или С")
    #print("Введите исходную шкалу температуры: ")
    inp = sys.argv[1]
    #print("Введите значение температуры: ")
    value = sys.argv[2]
    #print("Введите шкалу назначения: ")
    out = sys.argv[3]
    # print(inp, value, out)
    print(main(inp, float(value), out))
